function getCircleArea(radius){
    if(radius <= 0 || typeof radius != 'number'){
        return undefined
    }

    return 3.1416 * (radius**2);
}

function getNumberOfChar(char,string){
    /* let count = 0;
    for(let i = 0; i < string.length; i++){
        if(string[i].includes(letter)){
            count++;
        }
    }
    return count; */
    // transform the string into an array of characters so we can check the given char parameter against each character in the string parameter.
    // .split("") - splits the characters in a string and returns them in a new array
    if(typeof char != 'string' || typeof string != 'string'){
        return undefined;
    }

    let characters = string.split("");
    // console.log(characters);
    // loop over each Item in the characters array. If the char param matches with the current character being iterated we will increment the counter variable.
    let counter = 0;
    characters.forEach(character => {
        if(character === char){
            counter++
        }
    })
    // log the counter in the console
    return counter;
}

let users = [
    {
        username: "brBoyd87",
        password: "87brandon19"
    },
    {
        username: "tylerOfsteve",
        password: "stevenstyle75"
    }
]

module.exports = {
    getCircleArea,
    getNumberOfChar,
    users
}
