const { assert } = require('chai');
const { getCircleArea,getNumberOfChar } = require('../src/util')

describe('test_get_circle_area',() => {

    it('test_area_of_circle_radius_15_is_706.86',() => {
        let area = getCircleArea(15);
        assert.equal(area,706.86);
    });

    it('test_area_of_circle_neg1_radius_is_undefined',() => {

        let area = getCircleArea(-1);
        assert.isUndefined(area);
    });

    it('test_area_of_circle_0_radius_is_undefined',() => {
        let area = getCircleArea(0);
        assert.isUndefined(area);
    });

    it('test_area_of_circle_string_radius_is_undefined',() => {
        let area = getCircleArea('string');
        assert.isUndefined(area);
    });

    it('test_area_of_circle_invalid_type_is_undefined',() => {
        let area = getCircleArea(true);
        assert.isUndefined(area);
    });
});

describe('test_get_number_of_char_in_string',() => {

    it('test_number_of_l_in_string_is_3',() => {
        let numChar = getNumberOfChar("l","Hello World")
        assert.equal(numChar,3);
    });

    it('test_number_of_a_in_string_is_2',() => {
        let numChar = getNumberOfChar("a","Malta")
        assert.equal(numChar,2);
    })

    it('test_result_if_first_argument_given_is_not_a_string_is_undefined',() => {
        let numChar = getNumberOfChar(2,"string")
        assert.isUndefined(numChar);
    })

    it('test_result_if_second_argument_given_is_not_a_string_is_undefined',() => {
        let numChar = getNumberOfChar("a",true)
        assert.isUndefined(numChar);
    })

})
